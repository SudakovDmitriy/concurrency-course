#pragma once

#include <twist/stdlike/atomic.hpp>

#include <cstdlib>
#include <sys/syscall.h>
#include <linux/futex.h>
#include <unistd.h>

namespace stdlike {

using FutexWordType = int32_t;

class Mutex {
 public:
  void Lock() {
    FutexWordType cur_value = 0;
    if (!futex_word_.compare_exchange_strong(cur_value, 1)) {
      do {
        // We wait if
        // if cur_value is already 2 (at least one thread waits) 
        // or if cur_value is 1 and we can xchg it to 2
        //
        // cur_value cant be 0
        if (cur_value == 2 ||
            futex_word_.compare_exchange_strong(cur_value, 2)) {
          //futex_word_.FutexWait(2);
          syscall(SYS_futex, (FutexWordType*)&futex_word_, FUTEX_WAIT,
                            2, NULL, NULL, NULL);
        }

        cur_value = 0;
      } while (!futex_word_.compare_exchange_strong(cur_value, 2));
    }

  }

  void Unlock() {
    if (futex_word_.fetch_sub(1) != 1){
      futex_word_.store(0);
      //futex_word_.FutexWakeOne();
      syscall(SYS_futex, (FutexWordType*)&futex_word_, FUTEX_WAKE, 1, NULL,
              NULL, NULL);
    }
  }

 private:
  twist::stdlike::atomic<FutexWordType> futex_word_{0};
};

}  // namespace stdlike

