#pragma once

#include "tagged_semaphore.hpp"

#include <deque>

namespace solutions {

// Bounded Blocking Multi-Producer/Multi-Consumer (MPMC) Queue

template <typename T>
class BlockingQueue {
 private:
  using Token = typename TaggedSemaphore<T>::Token;
  using Guard = typename TaggedSemaphore<T>::Guard;

 public:
  explicit BlockingQueue(size_t capacity)
      : size_(0), capacity_(capacity), queue_access_(1) {
  }

  // Inserts the specified element into this queue,
  // waiting if necessary for space to become available.
  void Put(T value) {
    Token t(std::move(capacity_.Acquire()));

    {
      Guard guard = queue_access_.MakeGuard();
      q_.emplace_back(std::move(value));
    }

    size_.Release(std::move(t));
  }

  // Retrieves and removes the head of this queue,
  // waiting if necessary until an element becomes available
  T Take() {
    Token t(std::move(size_.Acquire()));

    Guard guard = queue_access_.MakeGuard();
    T res(std::move(q_.at(0)));
    q_.pop_front();

    capacity_.Release(std::move(t));

    return res;
  }

 private:
  std::deque<T> q_;
  TaggedSemaphore<T> size_;
  TaggedSemaphore<T> capacity_;

  TaggedSemaphore<T> queue_access_;
};

}  // namespace solutions
