#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
#include <cstdint>
#include "twist/stdlike/atomic.hpp"

namespace solutions {

// A Counting semaphore

// Semaphores are often used to restrict the number of threads
// than can access some (physical or logical) resource

class Semaphore {
 public:
  // Creates a Semaphore with the given number of permits
  explicit Semaphore(size_t tickets_count) : counter_(tickets_count) {
    // Not implemented
  }

  // Acquires a permit from this semaphore,
  // blocking until one is available
  void Acquire() {
    while (counter_.fetch_sub(1) <= 0) {
      {
        std::lock_guard lock(cv_mutex_);
        counter_.fetch_add(1);
      }
      cv_.notify_one();

      std::unique_lock sem_empty(cv_mutex_);
      cv_.wait(sem_empty, [this] {
        return counter_.load() > 0;
      });
    }
  }

  // Releases a permit, returning it to the semaphore
  void Release() {
    {
      std::lock_guard lock(cv_mutex_);
      counter_.fetch_add(1);
    }
    cv_.notify_one();
  }

 private:
  twist::stdlike::atomic<int> counter_;
  twist::stdlike::mutex cv_mutex_;
  twist::stdlike::condition_variable cv_;
  // Permits
};

}  // namespace solutions
