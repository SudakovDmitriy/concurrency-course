#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>
#include <twist/stdlike/atomic.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
#include <cstdint>
#include "twist/strand/stdlike/thread.hpp"
#include <iostream>

namespace solutions {

// CyclicBarrier allows a set of threads to all wait for each other
// to reach a common barrier point

// The barrier is called cyclic because
// it can be re-used after the waiting threads are released.

class CyclicBarrier {
 public:
  explicit CyclicBarrier(size_t participants)
      : can_go_(false), counter_(0), t_count_(participants) {
  }

  // Blocks until all participants have invoked Arrive()
  void Arrive() {
    {
      std::unique_lock barier_empty(cv_mutex_);
      // std::cout << "begin " <<
      // twist::strand::stdlike::this_thread::get_id()<< std::endl;
      cv_empty_.wait(barier_empty, [this] {
        return !can_go_.load();
      });
    }

    size_t prev = counter_.fetch_add(1);
    if (prev == t_count_ - 1) {
      {
        std::lock_guard lock_full(cv_mutex_full_);
        can_go_.store(true);
      }
      cv_full_.notify_all();
    }

    {
      std::unique_lock barier_full(cv_mutex_full_);
      //std::cout << "end " <<  twist::strand::stdlike::this_thread::get_id()<< std::endl;
      cv_full_.wait(barier_full, [this] {
        return can_go_.load();
      });
    }

    prev = counter_.fetch_sub(1);
    if (prev == 1) {
      {
        std::lock_guard lock_empty(cv_mutex_);
        can_go_.store(false);
      }
      cv_empty_.notify_all();
    }
  }

 private:
  twist::stdlike::atomic<bool> can_go_;
  twist::stdlike::atomic<size_t> counter_;
  const size_t t_count_;

  twist::stdlike::mutex cv_mutex_; 
  twist::stdlike::mutex cv_mutex_full_; 
  twist::stdlike::condition_variable cv_empty_;
  twist::stdlike::condition_variable cv_full_;
};

}  // namespace solutions
